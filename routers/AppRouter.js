import { createStackNavigator } from "react-navigation";
import HomePage from "../components/Home"
import CoinDetailsPage from "../components/CoinDetails"

const AppRouter = createStackNavigator({
    HomePage: HomePage,
    CoinDetails: CoinDetailsPage
  },
  {
    initialRouteName: 'HomePage',
    headerMode: 'none',
    navigationOptions: {
    headerVisible: false,
  }
 });

 export default AppRouter;