import { createSwitchNavigator } from "react-navigation";
import AppRouter from "./AppRouter";
import LoginScreen from "../components/Login"

export default createSwitchNavigator(
  {
    Login: LoginScreen,
    App: AppRouter
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  }
);
