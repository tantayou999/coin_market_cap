import { combineReducers } from 'redux';
import coinsReducer from './coinsReducer';

const reducers = {
    coins: coinsReducer
}
const allReducers= combineReducers(reducers);
export default allReducers;