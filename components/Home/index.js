import React from 'react';
import { StyleSheet, RefreshControl, FlatList } from 'react-native';
import { Container, Header, View, Left, Button, Content, Form, Item, Input, Label, Text, Icon, Card, CardItem, Body, Grid, Row, Right, ListItem } from 'native-base';
import { connect } from 'react-redux';
import { fetchCoins } from '../../actions'


const styles = StyleSheet.create({
  container:{
      paddingHorizontal: 20,
      paddingVertical: 10,
      flex: 1, 
      justifyContent:"flex-start"
  }
})

class HomePage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        refreshing: false,
        searchtext: ''
      };
    }
  
    _keyExtractor = (item, index) => '' + item.id;
    
    componentDidMount(){
      this.handleRefresh();
    }

    handleRefresh = async() => {;

      console.log("ref")
      this.setState({
        refreshing: true,
      });
      await this.makeRemoteRequest();
      console.log("refend")
      this.setState({
        refreshing: false,
      });
    }

    makeRemoteRequest = async() => {
      this.props.fetchCoins();
    };

    searchFilterFunction = (text,data) => {
      const newData = data.filter(item => {
        const itemData = `${item.name.toUpperCase()}`;
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;    
      });    
    
      return newData;
    };

    signOut = () => {
      this.props.navigation.navigate('Login');
    };

    render() {
      return (
        <Container>
          <Header searchBar rounded>
            <Item>
              <Icon name="ios-search" />
              <Input placeholder="Search" onChangeText={(text) => {this.setState({searchtext: text});}} />
              <Icon onPress={this.signOut} name="ios-log-out" />
            </Item>
          </Header>
          <FlatList 
            data={this.searchFilterFunction(this.state.searchtext, this.props.coins)}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) => (
              <ListItem onPress={() => {this.props.navigation.push('CoinDetails', {coin: item})}}>
                <Left>
                    <Text>{item.name}</Text>
                    <Text note> {item.symbol}</Text>
                  </Left>
                  <Body>
                    <Text note style={{textAlign: "right"}}>${item.quote.USD.price}</Text>
                  </Body>
              </ListItem>
            )}>
          </FlatList>
        </Container>
      );
    }
  }
  
const mapStateToProps = state => {
  return { coins: state.coins }
}
export default connect(mapStateToProps, { fetchCoins })(HomePage);
