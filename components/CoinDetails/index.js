import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, View, Button, Content, Left, Title, Form, Item, Input, Label, Text, Icon, Card, CardItem, Body, Grid, Row, Right, ListItem } from 'native-base';

const styles = StyleSheet.create({
    right: {
        textAlign: "right"
    }
  })

class CoinDetailsPage extends React.Component {
    constructor(props) {
      super(props);
    }

    render() {
      const coin = this.props.navigation.getParam('coin', {})
      console.log(coin)
      return (
        <Container>
        <Header>
          <Left>
            <Button onPress={() => this.props.navigation.goBack()} transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{coin.name}</Title>
          </Body>
          <Right>
          </Right>
        </Header>
        <Content>
        <ListItem>
            <Left><Text>Symbol</Text></Left>
            <Body><Text style={styles.right} note>{coin.symbol}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Market Cap</Text></Left>
            <Body><Text style={styles.right} note>{coin.quote.USD.market_cap}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Price</Text></Left>
            <Body><Text style={styles.right} note>${coin.quote.USD.price}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Circulating Supply</Text></Left>
            <Body><Text style={styles.right} note>{coin.circulating_supply}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Total Supply</Text></Left>
            <Body><Text style={styles.right} note>{coin.total_supply}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Max Supply</Text></Left>
            <Body><Text style={styles.right} note>{coin.max_supply}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>Number Of Market Pairs</Text></Left>
            <Body><Text style={styles.right} note>{coin.num_market_pairs}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>24h Trading Volume</Text></Left>
            <Body><Text style={styles.right} note>{coin.quote.USD.volume_24h}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>1h Percentage Change</Text></Left>
            <Body><Text style={styles.right} note>{coin.quote.USD.percent_change_1h}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>24h Percentage Change</Text></Left>
            <Body><Text style={styles.right} note>{coin.quote.USD.percent_change_24h}</Text></Body>
        </ListItem>
        <ListItem>
            <Left><Text>7d Percentage Change</Text></Left>
            <Body><Text style={styles.right} note>{coin.quote.USD.percent_change_7d}</Text></Body>
        </ListItem>
        </Content>
        </Container>
      );
    }
  }
  
export default CoinDetailsPage;
