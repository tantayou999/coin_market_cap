import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, View, Button, Content, Form, Item, Input, Label, Text, Icon, Card, CardItem, Body, Grid, Row } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const styles = StyleSheet.create({
  bottom: {
      flex: 1,
      justifyContent: 'flex-end',
  },
  button: {
      marginBottom: 10,
  },
  text: {
      marginBottom: 6,
      textAlign: 'center',
      fontSize: 12,
      fontWeight: 'bold'
    },
  label:{
      marginTop: 10, 
      fontWeight: 'bold', 
      marginLeft: 5
    },
  container:{
      paddingHorizontal: 20,
      paddingVertical: 10,
      flex: 1, 
      justifyContent:"flex-start"
    },
  header:{
      marginTop: 5, 
      fontSize: 24, 
      fontWeight: 'bold'
    }
})

class LoginScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        showButtons: true,
        username: '',
        password: '',
        error: '',
        loading: false
      };
    }
  
    signIn = () => {
        this.props.navigation.navigate('App');
    };
  
    render() {
      return (
        <Container style={styles.container}>
        <Grid>
          <Row style={{height: 50, flexDirection: "column"}}>
            <Text style={styles.header} >Log In</Text>
          </Row>
          <Row>
          <KeyboardAwareScrollView style={{height: "100%"}}>
          <Form>
            <Text style={styles.label}>Username</Text>
            <Input placeholder="Username"/>
            <Text style={styles.label}>Password</Text>
            <Input placeholder="Password" secureTextEntry={true}/>
          </Form>
          </KeyboardAwareScrollView>
          </Row>
          <Row style={{height: 100}}>
          <View style={styles.bottom}>
            <Button full style={styles.button} onPress={this.signIn}>
                <Text>LOG IN</Text>
            </Button>
          </View>
          </Row>
          </Grid>
        </Container>
      );
    }
  }
  
export default LoginScreen;
