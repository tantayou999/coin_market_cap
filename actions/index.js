import axios from 'axios'

export const fetchCoins = () => async ( dispatch, getState ) => {
    const response = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', {
            headers: {"X-CMC_PRO_API_KEY": "2acd767d-7644-458a-a8e1-8713ee74e9d4"} })
    dispatch({ type: 'FETCH_COINS', payload: response.data.data })
};