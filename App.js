/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createAppContainer, SafeAreaView } from 'react-navigation';
import allReducers from './reducers/index.js';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import MainSwitchRouter from "./routers/MainSwitchRouter";

const store = createStore(allReducers, applyMiddleware(thunk));
const AppNavigator = createAppContainer(MainSwitchRouter);

export default class App extends Component{
    render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
          <AppNavigator/>
        </SafeAreaView>
      </Provider>
    );
  }
}